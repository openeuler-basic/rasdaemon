Name:			rasdaemon
Version:		0.6.6
Release:		2
License:		GPLv2
Summary:		Utility to get Platform Reliability, Availability and Serviceability (RAS) reports via the Kernel tracing events
URL:			https://github.com/mchehab/rasdaemon.git
Source0:		https://github.com/mchehab/rasdaemon/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

ExcludeArch:		s390 s390x
BuildRequires:  	gcc, gettext-devel, perl-generators, sqlite-devel, systemd, git, libtool
Provides:		bundled(kernel-event-lib)
Requires:		hwdata
Requires:		perl-DBD-SQLite
%ifarch %{ix86} x86_64
Requires:		dmidecode
%endif

Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd

Patch1: bugfix-ras-events-memory-leak.patch
Patch2: bugfix-rasdaemon-wait-for-file-access.patch
Patch3: bugfix-fix-fd-check.patch

%description
The  rasdaemon  program  is  a  daemon which monitors the platform
Reliablity, Availability and Serviceability (RAS) reports from the
Linux kernel trace events. These trace events are logged in
/sys/kernel/debug/tracing, reporting them via syslog/journald.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoscan
aclocal
autoconf
autoheader
libtoolize --automake --copy --debug --force
automake --add-missing
%ifarch %{arm} aarch64
%configure --enable-mce --enable-aer --enable-sqlite3 --enable-extlog --enable-abrt-report --enable-devlink --enable-diskerror --enable-non-standard --enable-hisi-ns-decode --enable-arm
%else
%configure --enable-mce --enable-aer --enable-sqlite3 --enable-extlog --enable-abrt-report --enable-devlink --enable-diskerror
%endif
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
install -D -p -m 0644 misc/rasdaemon.env %{buildroot}%{_sysconfdir}/sysconfig/%{name}
install -D -p -m 0644 misc/rasdaemon.service %{buildroot}/%{_unitdir}/rasdaemon.service
install -D -p -m 0644 misc/ras-mc-ctl.service %{buildroot}%{_unitdir}/ras-mc-ctl.service
rm INSTALL %{buildroot}/usr/include/*.h

%files
%doc ChangeLog README TODO
%license AUTHORS COPYING
%{_sbindir}/rasdaemon
%{_sbindir}/ras-mc-ctl
%{_mandir}/*/*
%{_unitdir}/*.service
%{_sharedstatedir}/rasdaemon
%{_sysconfdir}/ras/dimm_labels.d
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}

%post
/usr/bin/systemctl enable rasdaemon.service >/dev/null 2>&1 || :

%changelog
* Fri Sep 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.6.6-2
- Update software source URL

* Fri Jul 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.6.6-1
- Update software to v0.6.6

* Tue Feb 25 2020 lvying<lvying6@huawei.com> - 0.6.3-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix file descriptor leak in ras-report.c:setup_report_socket()

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.6.3-1
- Package init
